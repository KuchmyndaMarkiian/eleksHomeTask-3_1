namespace HT1.Interfaces
{
    public interface ICountingBooks
    {
        int GetBookCount();
    }
}