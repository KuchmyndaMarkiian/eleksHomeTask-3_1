using System;
using System.Collections.Generic;
using HT1.Files;
using HT1.Interfaces;
using Other;
using _1_Library.Interfaces;

namespace HT1.Datas
{
    public class Author : IAuthor, IComparable<Author>
    {
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public Author(string name, List<Book> books)
        {
            Name = name;
            Books = books;
        }

        public int GetBookCount()
        {
            return Books.Count;
        }

        public int CompareTo(Author other)
        {
            if (GetBookCount() > other.GetBookCount())
                return 1;
            if (GetBookCount() < other.GetBookCount())
                return -1;
            return 0;
        }

        public override string ToString() => $"Author- {Name}:\n{Books.toString()}\n";
    }
}