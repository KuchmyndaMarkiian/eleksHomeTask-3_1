﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using static System.Console;

namespace Library.XMLClass
{
    public class XmlEditor
    {
        delegate void NodeAction(XmlNode node, XmlNode element, KeyValuePair<string, string>[] attributesNew);
        private readonly XmlDocument _document;
        public XmlEditor(string path = "Library.xml")
        {
            _document = new XmlDocument();
            _document.Load(Environment.CurrentDirectory.Replace(@"\bin\Debug", "\\") + path);
        }
        #region Add node

        public void PushNode(string elem, KeyValuePair<string, string>[] attributes, string intoElement)
        {
            XmlElement element = CreateElement(elem, attributes);
            AllNodes(element, intoElement, null, AppendChild);
        }

        void AppendChild(XmlNode intoNode, XmlNode element)
        {
            intoNode.AppendChild(element);
        }

        private void AppendChild(XmlNode node, XmlNode element, KeyValuePair<string, string>[] attributesNew)
        {
            AppendChild(node, element);
        }

        #endregion
        #region Replace node

        public void ReplaceNode(KeyValuePair<string, string>[] attributesNew, string intoElement)
        {
            AllNodes(null, intoElement, attributesNew, SetAttributes);
        }

        private void SetAttributes(XmlNode node, XmlNode element, KeyValuePair<string, string>[] attributesnew)
        {
            SetAttributes(attributesnew, node);
        }

        private static void SetAttributes(KeyValuePair<string, string>[] attributesNew, XmlNode node)
        {
            if (node.Attributes.Count > 0)
            {
                if (attributesNew.Any(x => x.Key == "name"))
                {
                    XmlNode attr = node.Attributes.GetNamedItem("name");
                    attr.Value = attributesNew.FirstOrDefault(x => x.Key == "name").Value;
                }
                //для книжки
                if (attributesNew.Any(x => x.Key == "pages"))
                {
                    XmlNode pages = node.Attributes.GetNamedItem("pages");
                    if (pages != null)
                    { pages.Value = attributesNew.FirstOrDefault(x => x.Key == "pages").Value;}
                }
            }
        }

        #endregion
        #region Remove node

        public void RemoveNode(string name)
        {
            AllNodes(null, name, null, RemoveNode);
        }

        private void RemoveNode(XmlNode node, XmlNode element, KeyValuePair<string, string>[] attributesnew)
        {
            node.RemoveChild(element);
        }

        #endregion
        private XmlElement CreateElement(string elem, KeyValuePair<string, string>[] attributes)
        {
            XmlElement element = _document.CreateElement(elem);
            foreach (var attribute in attributes)
            {
                XmlAttribute attr = _document.CreateAttribute(attribute.Key);
                attr.AppendChild(_document.CreateTextNode(attribute.Value));
                element.Attributes.Append(attr);
            }
            return element;
        }
        public void Save(string path = "Library.xml")
        {
            _document.Save(path);
        }
        public void PrintNodes()
        {
            var root = _document.DocumentElement;
            PrintNode(root, "Library");
            WriteLine();
            foreach (XmlNode node in root)
            {
                PrintNode(node, "Section");
                foreach (XmlNode section in node.ChildNodes)
                {
                    PrintNode(section, "Author");
                    foreach (XmlNode author in section.ChildNodes)
                    {
                        PrintNode(author, "Book");
                        foreach (XmlNode book in author.ChildNodes)
                        {
                            PrintNode(book, "Name");
                        }
                        WriteLine();
                    }
                    WriteLine();
                }
                WriteLine();
            }
        }
        private static void PrintNode(XmlNode node, string text)
        {
            if (node.Attributes.Count > 0)
            {
                XmlNode attr = node.Attributes.GetNamedItem("name");
                WriteLine($"{text}: {(attr != null ? attr.Value : " ")}");

                //для книжки
                XmlNode pages = node.Attributes.GetNamedItem("pages");
                if (pages != null)
                { WriteLine($"Pages: {pages.Value}");}
            }
        }
        void AggregateNodes(XmlNode node, XmlNode element, KeyValuePair<string, string>[] attributes,
            NodeAction action)
        {
            action(node, element, attributes);
        }
        void AllNodes(XmlElement element, string intoElement, KeyValuePair<string, string>[] attributes,
            NodeAction action)
        {
            var root = _document.DocumentElement;
            foreach (XmlNode node in root)
            {
                if (node.Attributes.GetNamedItem("name").Value == intoElement)
                {
                    if (action == RemoveNode)
                    {
                        AggregateNodes(root, node, attributes, action);
                    }
                    else
                    {
                        AggregateNodes(node, element, attributes, action);
                    }
                }
                foreach (XmlNode section in node.ChildNodes)
                {
                    if (section.Attributes.GetNamedItem("name").Value == intoElement)
                    {
                        if (action == RemoveNode)
                        {
                            AggregateNodes(node, section, attributes, action);
                        }
                        else
                        {
                            AggregateNodes(section, element, attributes, action);
                        }
                    }
                    foreach (XmlNode author in section.ChildNodes)
                    {
                        if (author.Attributes.GetNamedItem("name").Value == intoElement)
                        {
                            if (action == RemoveNode)
                            {
                                AggregateNodes(section, author, attributes, action);
                            }
                            else
                            {
                                AggregateNodes(author, element, attributes, action);
                            }
                        }
                        foreach (XmlNode book in author.ChildNodes)
                        {
                            if (book.Attributes?.GetNamedItem("name").Value == intoElement)
                            {
                                if (action == RemoveNode)
                                {
                                    AggregateNodes(author, book, attributes, action);
                                }
                                else
                                {
                                    AggregateNodes(book, element, attributes, action);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
