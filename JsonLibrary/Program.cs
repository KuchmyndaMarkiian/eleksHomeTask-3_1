﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System.Collections.Generic;
using HT1.Datas;
using HT1.Files;
using JsonLibrary.Json;
using JsonLibrary.Other;
using static System.Console;

namespace JsonLibrary
{
    static class Program
    {
        static void Main()
        {
            AbstractLibrary library = new Library("LPNU Library", new List<Section>
            {
                new Section("IT", new List<Author>
                {
                    new Author("Straustrup & Shieldt", new List<Book>
                    {
                        new Book("C++", 694),
                        new Book("C#", 534)
                    }),
                    new Author("Serdyuk", new List<Book>
                    {
                        new Book("C#", 534),
                        new Book("KPZ", 300),
                        new Book("MPZ", 350)
                    })
                }),
                new Section("Ukrainan Literature", new List<Author>
                    {
                        new Author("Taras Shevschenko", new List<Book> {new Book("Kobzar", 800)})
                    }
                ),
                new Section("Foreighn Literature", new List<Author>
                {
                    new Author("Oscar Wilde", new List<Book> {new Book("Dorian Gray", 1000)}),
                    new Author("Aziz Ansari", new List<Book> {new Book("How be single", 98)}),
                    new Author("Herbert Walles", new List<Book>
                    {
                        new Book("Future", 666),
                        new Book("Future #2", 466)
                    })
                })
            });

            WriteLine("IComparable test");
            {
                foreach (var sections in library.Sections)
                {
                    foreach (var authors in sections.Authors)
                    {
                        authors.Books.Sort();
                    }

                    sections.Authors.Sort();
                }
                library.Sections.Sort();
            }
            WriteLine(library.ToString());

            WriteLine("Serialization test");
            FileEditor editor=new FileEditor();
            editor.OpenOrCreate("D://file.txt");
            editor.Write(library.Serialize());
            WriteLine($"Serialization is finished {(editor.Flag?"sucessfull":"with errors")}");

            WriteLine("Deserialization test");
            editor = new FileEditor();
            editor.OpenOrCreate("D://file.txt");
            var deserialised = (AbstractLibrary) editor.Read().Deserialize(typeof(AbstractLibrary));
            WriteLine(object.Equals(deserialised,library));
            WriteLine($"Deserialization is finished {(editor.Flag ? "sucessfull" : "with errors")}");
            ReadKey();
        }
    }
}
